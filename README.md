#USAGE
* --warning|-w                        Warning threshold (ex. -10:0)
* --critical|-c                       Critical threshold (ex. 10)
* --graphite|-g                       Graphite server base URL (ex. http://graphite.local/)
* --metric|-m                         Set the Graphite metric name to check
* --tag|-t [tag name]                 Set an optional tag value for the metric. Used in the nagios output (default: "Value")
* --start-time|-s                     Set the series start time (default: now minus 15 min).
* --end-time|-e                       Set the series end time (default: now).
* --calc-mode|-a [last|avg|max|min]   Specify how to calculate the number that will be checked against thresholds (default: last)

**Example**
```
    $ check_graphite -w 5000 -c 20000 -g http://graphite.olx.com 
        -m "sumSeries(olx-production.application.mobile.pusher.*.*.providers.urbanAirship.queue.size)"
        -a max
```